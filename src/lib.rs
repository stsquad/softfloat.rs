/*
 * Pure Rust SoftFloat emulation.
 *
 * This code is based on the modern QEMU softfloat library written in
 * C, which in turn was evolved from the original BSD softfloat
 * library written by John R. Hauser. The QEMU refactor attempted to
 * move from the original bit-twiddling code to a common set of
 * functions that are shared cleanly between the various softfloat
 * types. The implementation uses a bit of C macro hackery but mostly
 * relies on the compiler being able to inline and expand code based
 * on constants known at compile time.
 */

pub(crate) mod utils;
use crate::utils::{ShiftableUnsigned, SoftfloatFracUtils};
use std::convert::{TryFrom, TryInto};

/*
 * Describes the type of floating point number.
 */
enum FloatClass {
    Unclassified,
    Zero,
    Normal,
    Infinity,
    QuiteNaN,  /* all NaNs from here */
    SignalingNan,
}

/*
 * Floating point number decomposed into parts.
 *
 */
struct FloatParts <T: ShiftableUnsigned> {
    class: FloatClass,
    sign: bool,
    exp: isize,
    frac: T,
}

/* Structure holding all of the relevant parameters for a format.
 *   exp_size: the size of the exponent field
 *   exp_bias: the offset applied to the exponent field
 *   exp_max: the maximum normalised exponent
 *   frac_size: the size of the fraction field
 *   frac_shift: shift to normalise the fraction with DECOMPOSED_BINARY_POINT
 * The following are computed based the size of fraction
 *   round_mask: bits below lsb which must be rounded
 * The following optional modifiers are available:
 *   arm_althp: handle ARM Alternative Half Precision
 */
struct FloatFmt <T: ShiftableUnsigned> {
    exp_size: isize,
    exp_bias: isize,
    exp_max: isize,
    frac_size: isize,
    frac_shift: isize,
    arm_alt_hp: bool,
    round_mask: T
}

const fn float_fmt <T: ShiftableUnsigned>
    (exp: isize, frac: isize, arm_alt_hp: bool)
     -> FloatFmt<T>
{
    let exp_bias = ((1 << exp) -1) >> 1;
    let exp_max = (1 << exp) -1;
    let frac_shift = match exp + frac {
        1..=63 => { (-frac - 1) & 63 }
        _ => { (-frac - 1) & 128 }
    };
    let round_mask = (T::one() << frac_shift) - T::one();

    FloatFmt::<T> {
        exp_size: exp,
        exp_bias,
        exp_max,
        frac_size: frac,
        frac_shift,
        arm_alt_hp,
        round_mask
    }
}

const FLOAT16_PARAMS: FloatFmt<u64> = float_fmt(5, 10, false);
const FLOAT16_PARAMS_AHP: FloatFmt<u64> = float_fmt(5, 10, true);
const BFLOAT16_PARAMS: FloatFmt<u64> = float_fmt(8, 7, false);
const FLOAT32_PARAMS: FloatFmt<u64> = float_fmt(8, 23, false);
const FLOAT64_PARAMS: FloatFmt<u64> = float_fmt(11, 52, false);
const FLOAT128_PARAMS: FloatFmt<u128> = float_fmt(15, 112, false);

/*
 * There are two types here - the input <I> which is the raw size of
 * the float and the fraction <F> which is the size needed to hold the
 * fraction.
 */
fn float_unpack <F: ShiftableUnsigned, I: SoftfloatFracUtils + ShiftableUnsigned>
    (fmt: FloatFmt<F>, raw: I)
     -> FloatParts<F>
    where isize: From<I>, F: From<I>
{
    let sign_bit = SoftfloatFracUtils::extract(raw, fmt.frac_size + fmt.exp_size, 1);
    let sign: bool = if sign_bit == I::one() { true } else { false };
    let exp: isize = SoftfloatFracUtils::extract(raw, fmt.frac_size,fmt.exp_size).try_into().unwrap();
    let frac: F = SoftfloatFracUtils::extract(raw, 0, fmt.frac_size).try_into().unwrap();

    FloatParts::<F> {
        class: FloatClass::Unclassified,
        sign,
        exp,
        frac
    }
}


#[cfg(test)]
mod tests {
     use super::*;

    #[test]
    fn basic_float32() {
        let raw_one: u32 = 0x3f80_0000;
        let one = float_unpack(FLOAT32_PARAMS, raw_one);
        assert_eq!(one.sign, false);

        let raw_neg_one: u32 = 0xbf80_0000;
        let neg_one = float_unpack(FLOAT32_PARAMS, raw_neg_one);
        assert_eq!(neg_one.sign, true);
    }
}
