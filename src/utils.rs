/*
 * SoftFloat Utility Functions
 */

use std::ops::{Shr, Shl, BitAnd, BitOrAssign};
use num_traits::Unsigned;
use std::convert::TryFrom;

/*
 * Jamming and extraction functions
 */

/*
 * We define the ShiftableUnsigned trait to be something that includes
 * all the other traits we'll need to implement our jamming function.
 * This saves us having to specify them all for the actual
 * implementation of the ShiftRightJam.
 */

pub trait ShiftableUnsigned: Unsigned
    + Shr<isize, Output=Self> + Shl<isize, Output=Self>
    + BitAnd<Output=Self> + BitOrAssign + Copy + Sized
{}

/*
 * Ensure we implement the trait but we don't need to add any more
 * methods.
 */

impl<T> ShiftableUnsigned for T where T: Unsigned
    + Shr<isize, Output=Self> + Shl<isize, Output=Self>
    + BitAnd<Self, Output=Self> + BitOrAssign + Copy + Sized
{}

/*
 * And now the declare the trait and the types it can apply to.
 */
pub trait SoftfloatFracUtils: ShiftableUnsigned {
    fn shrjam(frac: Self, shift: isize) -> Self;
    fn extract(frac: Self, start: isize, length: isize) -> Self;
}

/*
 * Finally a generic implementation. There are a few weirdnesses, the
 * main one being the use of T::one() and T::zero() to bring in
 * appropriately sized unsigned types which we shift and mask for
 * detecting the stuck bits.
 */
impl<T: ShiftableUnsigned> SoftfloatFracUtils for T
{
    fn shrjam(frac: T, shift: isize) -> T
    {
        let mut shift_frac: T;
        let bit_width = std::mem::size_of::<T>() * 8;
        let max_shift = isize::try_from(bit_width).expect("should work");
        if shift != 0 {
            if shift < max_shift {
                let bmask: T = (T::one() << shift) - T::one();
                let bottom_bits: T = frac.bitand(bmask);
                shift_frac = frac >> shift;
                if bottom_bits != T::zero() {
                    shift_frac |= T::one();
                }
            } else {
                /* bottom bit set if any original bits */
                shift_frac = if frac != T::zero() {
                    T::one()
                } else {
                    T::zero()
                }
            }
        } else {
            shift_frac = frac;
        }
        shift_frac
    }

    /* extract bit field from fraction */
    fn extract(frac: T, start: isize, length: isize) -> T
    {
        let bit_width = std::mem::size_of::<T>() * 8;
        let max_shift = isize::try_from(bit_width).expect("should work");
        let mask = (T::one() << length) - T::one();

        assert!(start >= 0 && length > 0 && length <= max_shift - start);

        return (frac >> start) & mask;
    }

}


#[cfg(test)]
mod tests {
    use super::{SoftfloatFracUtils};

    #[test]
    fn shift_right_jam32() {
        let pattern: u32 = 0b1101_0001_0000_0000;
        assert_eq!(u32::shrjam(pattern, 1),  0b0110_1000_1000_0000);
        assert_eq!(u32::shrjam(pattern, 4),  0b0000_1101_0001_0000);
        assert_eq!(u32::shrjam(pattern, 8),  0b0000_0000_1101_0001);
        assert_eq!(u32::shrjam(pattern, 9),  0b0000_0000_0110_1001);
        assert_eq!(u32::shrjam(pattern, 10), 0b0000_0000_0011_0101);
        assert_eq!(u32::shrjam(pattern, 11), 0b0000_0000_0001_1011);
        assert_eq!(u32::shrjam(pattern, 12), 0b0000_0000_0000_1101);
        assert_eq!(u32::shrjam(pattern, 13), 0b0000_0000_0000_0111);
        assert_eq!(u32::shrjam(pattern, 14), 0b0000_0000_0000_0011);
        assert_eq!(u32::shrjam(pattern, 15), 0b0000_0000_0000_0001);
        assert_eq!(u32::shrjam(pattern, 16), 0b0000_0000_0000_0001);
        assert_eq!(u32::shrjam(pattern, 32), 0b0000_0000_0000_0001);
        assert_eq!(u32::shrjam(pattern, 33), 0b0000_0000_0000_0001);
    }

    #[test]
    fn shift_right_jam64() {
        let pattern: u64 = 0b1101_0001_0000_0000_1101_0001_1100_1100;
        assert_eq!(u64::shrjam(pattern, 1),  0b0110_1000_1000_0000_0110_1000_1110_0110);
        assert_eq!(u64::shrjam(pattern, 4),  0b0000_1101_0001_0000_0000_1101_0001_1101);
        assert_eq!(u64::shrjam(pattern, 8),  0b0000_0000_1101_0001_0000_0000_1101_0001);
        assert_eq!(u64::shrjam(pattern, 32), 0b0000_0000_0000_0000_0000_0000_0000_0001);
        assert_eq!(u64::shrjam(pattern, 64), 0b0000_0000_0000_0000_0000_0000_0000_0001);
    }

    #[test]
    fn shift_right_jam128() {
        let pattern: u128 = 0x8421_8421_8421_8421_8421_8421_8421_8421;
        assert_eq!(u128::shrjam(pattern, 4),  0x0842_1842_1842_1842_1842_1842_1842_1843);
        assert_eq!(u128::shrjam(pattern, 16), 0x0000_8421_8421_8421_8421_8421_8421_8421);
        assert_eq!(u128::shrjam(pattern, 32), 0x0000_0000_8421_8421_8421_8421_8421_8421);
        assert_eq!(u128::shrjam(pattern, 33), 0x0000_0000_4210_c210_c210_c210_c210_c211);
    }

    #[test]
    fn extract32() {
        let pattern: u32 = 0b1101_0111_1010_0101_1011_1101_1100_0011;
        assert_eq!(u32::extract(pattern, 0, 4), 0b0011);
        assert_eq!(u32::extract(pattern, 4, 4), 0b1100);
        assert_eq!(u32::extract(pattern, 15, 3), 0b011);
    }

    fn extract64() {
        let pattern: u64 = 0x1234_5678_9ABC_DEF0;
        assert_eq!(u64::extract(pattern, 0, 4), 0x0);
        assert_eq!(u64::extract(pattern, 4, 4), 0xf);
        assert_eq!(u64::extract(pattern, 15, 3), 0b101);
    }

}
